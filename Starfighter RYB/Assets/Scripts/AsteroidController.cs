﻿using System;
using StarfighterRYB;
using UnityEngine;

public class AsteroidController : MonoBehaviour {
    public float Speed;
    public float RedHealth, YellowHealth, BlueHealth;
    private float _maxRedHealth, _maxYellowHealth, _maxBlueHealth;
    private HealthBar RedHealthBar, YellowHealthBar, BlueHealthBar;

    public int ScoreGiven;
    // Use this for initialization
    void Start()
    {
        _maxRedHealth = RedHealth;
        _maxYellowHealth = YellowHealth;
        _maxBlueHealth = BlueHealth;

        var healthBars = GetComponentsInChildren<HealthBar>();
        for (var i = healthBars.Length - 1; i >= 0; i--){
            var healthBar = healthBars[i];
            switch (healthBar.gameObject.name)
            {
                case "Red":
                    if (_maxRedHealth > 0)
                    {
                        RedHealthBar = healthBar;
                    }
                    else
                    {
                        Destroy(healthBar.gameObject);
                    }
                    break;
                case "Yellow":
                    if (_maxYellowHealth > 0)
                    {
                        YellowHealthBar = healthBar;
                    }
                    else
                    {
                        Destroy(healthBar.gameObject);
                    }
                    break;
                case "Blue":
                    if (_maxBlueHealth > 0)
                    {
                        BlueHealthBar = healthBar;
                    }
                    else
                    {
                        Destroy(healthBar.gameObject);
                    }
                    break;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += Vector3.down * Speed * Time.deltaTime;
    }

    public void TakeDamage(float damagePoints, AttackColor color)
    {
        if (color == AttackColor.None) {
            return;
        }

        ScoreManager.AddScore(Convert.ToInt32(damagePoints));

        switch (color){
            case AttackColor.Red:
                RedHealth -= damagePoints;
                if (RedHealth < 0){
                    RedHealth = 0;
                }

                if (_maxRedHealth > 0)
                {
                    RedHealthBar.SetSize(RedHealth / _maxRedHealth);
                }

                break;
            case AttackColor.Yellow:
                YellowHealth -= damagePoints;
                if (YellowHealth < 0) {
                    YellowHealth = 0;
                }

                if (_maxYellowHealth > 0)
                {
                    YellowHealthBar.SetSize(YellowHealth / _maxYellowHealth);
                }

                break;
            case AttackColor.Blue:
                BlueHealth -= damagePoints;
                if (BlueHealth < 0) {
                    BlueHealth = 0;
                }

                if (_maxBlueHealth > 0)
                {
                    BlueHealthBar.SetSize(BlueHealth / _maxBlueHealth);
                }

                break;
        }

        if ((RedHealth+YellowHealth+BlueHealth) <= 0)
        {
            Died();
        }
    }

    private void Died()
    {
        ScoreManager.AddScore(ScoreGiven);
        LevelManager.Instance.LevelCompletedAmount++;
        Destroy(gameObject);
    }
}