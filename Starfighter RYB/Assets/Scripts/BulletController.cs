﻿using StarfighterRYB;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public float Speed;
    public float DamageStrength;
    private AttackColor _attackColor;

    // Use this for initialization
    void Start () {
        
    }
    
    // Update is called once per frame
    void Update ()
    {
        transform.position += Vector3.up*Speed*Time.deltaTime;

        if (transform.position.y >= 25f)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Asteroids")
        {
            AsteroidController controller = other.gameObject.GetComponent<AsteroidController>();
            controller.TakeDamage(DamageStrength, _attackColor);
        }

        Destroy(gameObject);
    }

    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    public void SetColor(AttackColor color){
        _attackColor = color;
    }
}
