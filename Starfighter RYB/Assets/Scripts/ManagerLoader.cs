﻿using UnityEngine;

public class ManagerLoader : MonoBehaviour
{
    public LaneManager LaneManager;
    public AsteroidManager AsteroidManager;
    public LevelManager LevelManager;

    private void Awake()
    {
        if (LaneManager.Instance == null)
        {
            Instantiate(LaneManager);
        }

        if (AsteroidManager.Instance == null)
        {
            Instantiate(AsteroidManager);
        }

        if (LevelManager.Instance == null) {
            Instantiate(LevelManager);
        }
    }
}
