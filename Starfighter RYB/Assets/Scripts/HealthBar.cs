﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour {
    private float _baseX;
    public void Start()
    {
        _baseX = transform.position.x;
    }

    public void SetSize(float sizeNormalized) {
        if (sizeNormalized <= 0) {
            return;
        }

        // Position -0.5f (x) at 0 (scale), 0f (x) at 1 (scale)
        transform.localScale = new Vector3(sizeNormalized, 2f);
        var x = _baseX + (sizeNormalized / 2f - 0.5f);
        transform.position = new Vector3(x, transform.position.y, transform.position.z);
    }

    public void SetColor(Color color) {
        transform.Find("BarSprite").GetComponent<SpriteRenderer>().color = color;
    }
}
