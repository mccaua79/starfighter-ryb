﻿using UnityEngine;
using System.Collections;

public class BackgroundScroller : MonoBehaviour
{
    public GameObject[] Tiles;
    public float ScrollSpeed;
    public float TileSize;

    private Vector3[] _startPosition;

    void Start()
    {
        int length = Tiles.Length;
        _startPosition = new Vector3[length];
        for (int i = 0; i < length; i++)
        {
            GameObject obj = Tiles[i];
            _startPosition[i] = obj.transform.position;
        }
    }

    void Update()
    {
        int length = Tiles.Length;
        for (int i = 0; i < length; i++)
        {
            GameObject obj = Tiles[i];
            float newPosition = Mathf.Repeat(Time.time * ScrollSpeed, TileSize);
            obj.transform.position = _startPosition[i] + Vector3.down * newPosition;
        }
    }
}