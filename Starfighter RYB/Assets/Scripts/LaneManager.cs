﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LaneManager : MonoBehaviour
{
    public int NumberOfLanes = 1;
    private int _oldNumberOfLanes = 0;
    public int CurrentLane = 1;
    public GameObject Lane;
    private IList<GameObject> _laneLines = new List<GameObject>();

    public static LaneManager Instance = null;

    //Awake is always called before any Start functions
    void Awake()
    {
        //Check if instance already exists
        if (Instance == null)
        {
            //if not, set instance to this
            Instance = this;
        }
        //If instance already exists and it's not this:
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        CenterCurrentLane();
    }
    
    void FixedUpdate()
    {
        // Clear out existing lines, or just update the positions on NumberOfLanes change
        if (_oldNumberOfLanes != NumberOfLanes)
        {
            foreach (var line in _laneLines)
            {
                Destroy(line);
            }

            _laneLines.Clear();

            BuildLane(0);

            for (int i = 1; i <= NumberOfLanes; i++)
            {
                BuildLane(i);
            }

            _oldNumberOfLanes = NumberOfLanes;
        }
    }

    private void BuildLane(int lane)
    {
        var x = GetLaneX(lane)+0.5f;
        var newLane = Instantiate(Lane, Vector3.zero, Quaternion.identity);
        var lineRenderer = newLane.GetComponent<LineRenderer>();
        lineRenderer.startColor = new Color(255, 255, 255, 0);
        lineRenderer.endColor = new Color(255, 255, 255, 1f);
        lineRenderer.SetPositions(new[] { new Vector3(x, 5, 0), new Vector3(x, -5, 0) });
        // When the NumberOfLanes changes, recalculate the X position of each line
        _laneLines.Add(newLane);
    }

    public void AddLane()
    {
        NumberOfLanes++;
    }

    public void MoveLeft()
    {
        CurrentLane--;
        CheckLane();
    }

    public void MoveRight()
    {
        CurrentLane++;
        CheckLane();
    }

    public void CheckLane()
    {
        if (CurrentLane <= 1)
        {
            CurrentLane = 1;
        }
        else if (CurrentLane >= NumberOfLanes)
        {
            CurrentLane = NumberOfLanes;
        }

        Debug.Log("Current lane changed: " + CurrentLane);
    }

    private float GetLaneX(int lane)
    {
        var maxLanes = NumberOfLanes;
        var x = ((maxLanes - 1) * -0.5f) + (lane - 1);
        return x;
    }

    public float GetPlayerPositionX()
    {

        var x = GetLaneX(CurrentLane);
        return x;
    }

    private void CenterCurrentLane(){
        var adjustedLane = (int)Math.Round(NumberOfLanes / 2.0f,MidpointRounding.AwayFromZero );
        CurrentLane = adjustedLane;
        CheckLane();
    }
}
