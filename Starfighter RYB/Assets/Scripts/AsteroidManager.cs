﻿using UnityEngine;

public class AsteroidManager : MonoBehaviour
{
    public float SpawnTime;
    public GameObject Asteroid;

    public static AsteroidManager Instance = null;

    private int AsteroidsSpawned = 0;
    private LaneManager _laneManager;

    //Awake is always called before any Start functions
    void Awake()
    {
        //Check if instance already exists
        if (Instance == null)
        {
            //if not, set instance to this
            Instance = this;
        }
        //If instance already exists and it's not this:
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    void Start () {
		InvokeRepeating("SpawnAsteroid", SpawnTime, SpawnTime);
        _laneManager = LaneManager.Instance;
    }

    void SpawnAsteroid()
    {
        var levelManager = LevelManager.Instance;
        AsteroidsSpawned++;
        var maxLanes = _laneManager.NumberOfLanes;
        var lane = Random.Range(1, maxLanes+1);
        var x = ((maxLanes - 1)*-0.5f) + (lane - 1);
        var asteroid = Instantiate(Asteroid, new Vector3(x, 7, 0), Quaternion.identity);
        if (asteroid != null) {
            var controller = asteroid.GetComponent<AsteroidController>();
            var hp = Random.Range(levelManager.AsteroidMinHP, levelManager.AsteroidMaxHP);
            controller.RedHealth = hp;
        }

        // Don't spawn any more asteroids after the max for the level has been reached.
        // There will be a timeframe of asteroids on screen before the player has destroyed them all
        if (AsteroidsSpawned >= levelManager.TotalToCompleteLevel) {
            CancelInvoke("SpawnAsteroid");
        }
    }
}
