﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    private static int _score, _highScore; // The player's score.

    public Text TxtScore;


    public void Awake()
    {
        // Reset the score.
        _score = 0;
        _highScore = PlayerPrefs.GetInt("HighScore");
    }


    public void Update()
    {
        // Set the displayed text to be the word "Score" followed by the score value.
        TxtScore.text = $"{_score}";
    }

    public static int GetScore()
    {
        return _score;
    }

    public static int GetHighScore()
    {
        return _highScore;
    }
    public static void AddScore(int score)
    {
        _score += score;
    }

    public static void PostScore()
    {
        var oldHighScore = PlayerPrefs.GetInt("HighScore");
        if (oldHighScore < _score)
        {
            _highScore = _score;
            PlayerPrefs.SetInt("HighScore", _highScore);
        }
    }
}
