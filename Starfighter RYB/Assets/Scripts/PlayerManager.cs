﻿using StarfighterRYB;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerManager : MonoBehaviour
{
    public GameObject Bullet;
    public Transform FirePoint;

    private LaneManager _laneManager;
    private AttackColor _attackColor = AttackColor.None;

    // Use this for initialization
    private void Start()
    {
        _laneManager = LaneManager.Instance;
        transform.position = new Vector2(_laneManager.GetPlayerPositionX(), transform.position.y);
    }

    // Update is called once per frame
    private void Update()
    {
    }

    public void MoveUp()
    {
    }

    public void MoveDown()
    {

    }

    public void MoveLeft()
    {
        _laneManager.MoveLeft();
        transform.position = new Vector2(_laneManager.GetPlayerPositionX(), transform.position.y);
    }

    public void MoveRight()
    {
        _laneManager.MoveRight();
        transform.position = new Vector2(_laneManager.GetPlayerPositionX(), transform.position.y);
    }

    public void Attack()
    {
        var bullet = Instantiate(Bullet, FirePoint.position, Quaternion.identity);
        var bulletController = bullet.GetComponent<BulletController>();
        bulletController.SetColor(_attackColor);

    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Asteroids")
        {
            SceneManager.LoadScene(0);
        }
    }

    public void SetColor(int color)
    {
        _attackColor = (AttackColor)color;
    }
}