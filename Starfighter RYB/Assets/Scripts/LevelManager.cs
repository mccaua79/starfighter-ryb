﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public int CurrentLevel = 1;
    /// <summary>
    /// Amount of asteroids the player has destroyed, not to be confused with how many should be in the level total
    /// </summary>
    public float LevelCompletedAmount = 0;
    public float TotalToCompleteLevel = 0; // TODO: Calculate this based on which level you are on
    public int AsteroidMinHP = 1;
    public int AsteroidMaxHP = 1;
    public float AsteroidSpawnRate = 3;

    public static LevelManager Instance = null;

    //Awake is always called before any Start functions
    void Awake()
    {
        //Check if instance already exists
        if (Instance == null)
        {
            //if not, set instance to this
            Instance = this;
        }
        //If instance already exists and it's not this:
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        CalculateTotalToComplete(1);
    }

    void Update() {
        if (LevelCompletedAmount >= TotalToCompleteLevel) {
            // TODO: Trigger "Level Completed"
            Debug.Log($"Level {CurrentLevel} Completed");
            Pause();
        }
    }

    public void Pause()
    {
            Time.timeScale = 0; // Pause
    }

    public void BuildLevel(int level) {
        CalculateTotalToComplete(level);
    }

    private void CalculateTotalToComplete(int level){
        TotalToCompleteLevel = 10; // AsteroidsInLevel
        AsteroidMinHP = 1;
        AsteroidMaxHP = 5;
        CurrentLevel = level;

        // TODO: CurrentLevel, AsteroidsToSpawnInLevel, AsteroidMinRangeHP, AsteroidMaxRangeHP
    }
}
