﻿using System;
namespace StarfighterRYB
{
    public enum AttackColor
    {
        None = 0,
        Red = 1,
        Yellow = 2,
        Blue = 3
    }
}
